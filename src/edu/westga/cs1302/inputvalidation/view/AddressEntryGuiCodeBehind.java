package edu.westga.cs1302.inputvalidation.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * The Class AddressEntryGuiCodeBehind.
 * 
 * @author CS1302
 */
public class AddressEntryGuiCodeBehind {

	@FXML
	private TextField nameTextField;

	@FXML
	private TextField addressTextField;

	@FXML
	private TextField cityTextField;

	@FXML
	private TextField stateTextField;

	@FXML
	private Label stateRequiredFormatLabel;

	@FXML
	private TextField zipTextField;

	@FXML
	private Label zipRequiredFormatLabel;

	@FXML
	private TextField phoneTextField;

	@FXML
	private Label phoneRequiredFormatLabel;

	/**
	 * Instantiates a new address entry gui code behind.
	 */
	public AddressEntryGuiCodeBehind() {

	}

	@FXML
	private void initialize() {

	}

}
